const express = require('express')
var app = express()
const path = require('path')
const PORT = process.env.PORT || 5002

app.use(express.static(path.join(__dirname, '.')))
app.set('views', path.join(__dirname, 'html'))
app.set('view engine', 'html')

app.get('/', (req, res) => res.render('index'))

app.listen(PORT, () => console.log('Client running on port ' + PORT))
