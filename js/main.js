const SERVER_URL = "https://dictionnaire-serveur.herokuapp.com/"
// const SERVER_URL = "http://localhost:5001/"
const URL_REQUEST = "%s?rela=%i&tri=%s&type=%c"
const URL_AUTOCOMPLETE = "autocomplete?mot=%s"
const URL_HISTORIQUE = "historique"
const URL_RELATION = "relation"
var mots_autocomplete = []
var relations = []
var historique = []
var autocomplete_request;

// Charge l'historique et les relations au lancement de la page
window.onload = function() {
    new autoComplete({
        selector: 'input[name="champ-recherche"]',
        minChars: 3,
        delay: 250,
        source: function(term, response){
            try { autocomplete_request.abort()} catch(e) {}
            getAutocompletion(term, response)
        }
    });
    getRelation()
    getHistorique()
};

// Lie le champs input du mot à la fonction qui lance les requêtes
function recupererInfos(motCherche) {
    clearAll()
    try { autocomplete_request.abort()} catch(e) {}
    let mot = motCherche || document.getElementById('champ-recherche').value
    mot = mot.replace(/ > /g, ">")
    // Pour la recherche par relation
    let relation = document.getElementById('champ-recherche-relation').value
    if (relation !== undefined && relation != -1){
        chargementRelation(mot, relation)
    } else {
        chargementDonnees(mot)
    }
}

// Fonction qui prend un mot en paramètre et qui requête jeux de mot
// Alpha = true -> ordre alphabétique, sinon ordre par poids
function chargementDonnees(mot){
    console.log("chargement")
    
    document.getElementById('champ-recherche').value = mot
    document.getElementById('affichage0').checked = true
    document.getElementById('affichage1').checked = false

    let alpha = document.querySelector('input[name="tri"]:checked').value

    let url = SERVER_URL+URL_REQUEST.replace('%s', mot).replace('%i', -1)
    if (alpha == "1"){
        url = url.replace('%s','alpha')
    }

    // Mot et Définition
    let request = new XMLHttpRequest();
    request.open('GET', url, true)
    
    request.onload = function() {
        getHistorique()
        // Cas ou le mot n'existe pas
        if (traiterTexte(request.responseText,'erreur', false) != ""){
            let all_suggestions = request.responseText
                .replace(/\"/g,'')
                .replace(/\[/g,'')
                .replace(/\]/g,'')
            let suggestions = traiterTexte(all_suggestions, 'suggestions', true)
            createCard("erreur", "erreur", "Le mot " + mot + " n'a pas été trouvé. Voici des suggestions", suggestions, "Les mots qu'il est possible que vous ayez voulu écrire")
        } else {

            // Définition
            let capitalized_mot = mot[0].toUpperCase() + mot.slice(1)
            let definition = traiterTexte(request.responseText, 'definition', false)
            definition = definition.replace(/\n/g, '<br/>')
            createCard("informations", "definition", capitalized_mot, definition, "La définition du mot recherché")

            // Requêtage du reste des relations
            requestAll(mot, alpha)
        }
    }
    request.send(null)
}

function chargementRelation(mot, relation){
    console.log("chargement relation")
    document.getElementById('champ-recherche').value = mot
    document.getElementById('champ-recherche-relation').value = relation
    document.getElementById('affichage0').checked = true
    document.getElementById('affichage1').checked = false

    let alpha = document.querySelector('input[name="tri"]:checked').value

    let url = SERVER_URL+URL_REQUEST.replace('%s', mot).replace('%i', relation).replace('%c','r')
    if (alpha == "1"){
        url = url.replace('%s','alpha')
    }

    let request = new XMLHttpRequest();
    request.open('GET', url, true)

    request.onload = function() {
        /* Cas ou le mot n'existe pas */
        if(traiterTexte(request.responseText,'erreur', false) != ""){
            let all_suggestions = request.responseText
                .replace(/\"/g,'')
                .replace(/\[/g,'')
                .replace(/\]/g,'')
            let suggestions = traiterTexte(all_suggestions, 'suggestions', true)
            createCard("erreur", "erreur", "Le mot " + mot + "n'a pas été trouvé. Voici des suggestions", suggestions, "Les mots qu'il est possible que vous ayez voulu écrire")
        } else {
            let entites = traiterTexte(request.responseText, relation+"_entites", true)
            let def = traiterTexte(request.responseText, "relation_type", false)
            createCard("informations", "definition", "Termes ayant " + mot + " comme relation " + relation, entites, def)
        }
    }
    request.send(null)

}

// Nettoye le html des sections
function clearAll(){
    let infos = document.getElementById('informations')
    infos.innerHTML = ""
    infos.innerText = ""

    let erreurs = document.getElementById('erreur')
    erreurs.innerHTML = ""
    erreurs.innerText = ""

    let historique = document.getElementById('historique')
    historique.innerHTML = ""
    historique.innerText = ""

    let donnees = document.getElementById('accordion')
    donnees.innerHTML = ""
    donnees.innerText = ""
}

function requestAll(mot, alpha){
    console.log("requestAll")
    relations.forEach(relation => {
        let relation_split = relation.split(';')
        let relation_num = relation_split[0]
        let relation_name = relation_split[1]
        let url = SERVER_URL+URL_REQUEST.replace('%s', mot).replace('%i', relation_num)
        if (alpha == "1"){
            url = url.replace('%s','alpha')
        }
        let request = new XMLHttpRequest();
        request.open('GET', url, true)
        request.onload = function() { 
            let entites = traiterTexte(request.responseText, relation_num+"_entites", true)
            if (entites != ""){
                let def = traiterTexte(request.responseText, "relation_type", false)
                createCard("accordion", relation_num, relation_name, entites, def)
            }
        };
        request.send()
    })
}

// Traite le texte en enlevant les balise et en le transformant en lien si besoin
function traiterTexte(texte, balise, html){
    if (texte.search("<"+balise.toString()+">") != -1){
        debut = texte.search("<"+balise.toString()+">")+balise.length+2
        fin = texte.search("</"+balise.toString()+">")
        texte = texte.slice(debut,fin) //.replace(' ', '')
        if (texte != " " && texte != ""){
            if (html){
                let text_splitted = texte.split(',')
                return addLinks(text_splitted)
            } else {
                return texte
            }
        }else{
            return ""
        }
    }else{
        return ""
    }
}

// Ajoute les liens cliquables au texte
function addLinks(text){
    let linked_text = []
    text.forEach(mot =>{
        if (!isBlank(mot)){
            if (mot.search('</font>') != -1){
                linked_text.push('<a href=# onclick="recupererInfos(\''+mot.substring(18,mot.search('</font>'))+'\')">'+mot+'</a>')
            }else{
                linked_text.push('<a href=# onclick="recupererInfos(\''+mot+'\')">'+mot+'</a>')
            }
        }
    })
    return linked_text.toString().replace(/,/g, ", ")
}

// Affichage des entités et relations réponse 
function createCard(parentId, relation_code, relation_name, entites, tooltip_text){
    let new_card = document.createElement("div")
    new_card.setAttribute("class", "card")

    // Split des entites
    let entites_split = entites.split(",")
    
    // header
    let new_card_header = document.createElement("div")
    new_card_header.setAttribute("id", "heading-"+relation_code)
    new_card_header.setAttribute("class", "card-header")
    new_card_header.setAttribute("data-toggle", "tooltip")
    new_card_header.setAttribute("data-placement", "right")
    new_card_header.setAttribute("title", tooltip_text)
    
    let new_card_header_name = document.createElement("h2")
    new_card_header_name.setAttribute("class", "mb-0")
    let new_card_header_button = document.createElement("button")
    if(entites == "") {
        new_card_header_button.setAttribute("class", "btn btn-link disabled")
    } else {
        new_card_header_button.setAttribute("class", "btn btn-link")
    }
    new_card_header_button.setAttribute("data-toggle", "collapse")
    new_card_header_button.setAttribute("data-target", "#collapse-"+relation_code)
    new_card_header_button.setAttribute("aria-expanded", "false")
    new_card_header_button.setAttribute("aria-controls", "collapse-"+relation_code)
    new_card_header_button.innerHTML = relation_name

    new_card_header_name.appendChild(new_card_header_button)
    new_card_header.appendChild(new_card_header_name)
    new_card.appendChild(new_card_header)

    // body
    let new_card_body = document.createElement("div")
    new_card_body.setAttribute("id", "collapse-"+relation_code)
    new_card_body.setAttribute("class", "collapse")
    new_card_body.setAttribute("aria-labelledby", "heading-"+relation_code)
    new_card_body.setAttribute("data-parent", "#"+parentId)
    let new_card_body_text = document.createElement("div")
    new_card_body_text.setAttribute("class", "card-body")
    
    // 2 div : une pour le texte en ligne, une pour le texte en liste
    // Ligne
    let new_card_body_text_raw = document.createElement("div")
    new_card_body_text_raw.setAttribute("name", "raw")
    new_card_body_text_raw.setAttribute("id", "raw_"+relation_code)
    new_card_body_text_raw.style.display = "block"
    // si entites_split.length > 10
    let new_card_body_text_raw_less = document.createElement("span")
    new_card_body_text_raw_less.setAttribute("id","less_"+relation_code)
    
    let new_card_body_text_raw_dots = document.createElement("span")
    new_card_body_text_raw_dots.setAttribute("id","dots_"+relation_code)
    new_card_body_text_raw_dots.style.display = "inline"

    let new_card_body_text_raw_more = document.createElement("span")
    new_card_body_text_raw_more.setAttribute("id","more_"+relation_code)
    new_card_body_text_raw_more.style.display = "none"

    let new_card_body_text_raw_button = document.createElement("button")
    new_card_body_text_raw_button.setAttribute("id","button_"+relation_code)
    new_card_body_text_raw_button.setAttribute("value",relation_code);
    new_card_body_text_raw_button.addEventListener("click", (x) => readMore(relation_code));
    new_card_body_text_raw_button.setAttribute("class","btn btn-primary")
    let new_card_body_text_raw_button_text = document.createTextNode("Voir plus");

    let br_section = document.createElement("p")
    br_section.innerHTML = ""

    // Colonnes
    let new_card_body_text_list = document.createElement("div")
    new_card_body_text_list.setAttribute("name", "list")
    new_card_body_text_list.setAttribute("id", "list_"+relation_code)
    new_card_body_text_list.style.display = "none"

    let new_card_body_text_list_data = document.createElement("div")
    new_card_body_text_list_data.setAttribute("id", "list_data_"+relation_code)

    if (entites_split.length > 10){
        new_card_body_text_raw_button.appendChild(new_card_body_text_raw_button_text);  
        new_card_body_text_raw.appendChild(new_card_body_text_raw_less)
        new_card_body_text_raw.appendChild(new_card_body_text_raw_dots)
        new_card_body_text_raw.appendChild(new_card_body_text_raw_more)  
        new_card_body_text_raw.appendChild(br_section)
        new_card_body_text_raw.appendChild(new_card_body_text_raw_button)   
    }
    new_card_body_text.appendChild(new_card_body_text_raw)
    new_card_body_text_list.appendChild(new_card_body_text_list_data)
    new_card_body_text.appendChild(new_card_body_text_list)
    new_card_body.appendChild(new_card_body_text)
    new_card.appendChild(new_card_body)

    document.getElementById(parentId).appendChild(new_card)

    // En cas de définition, ne rien faire
    if (relation_code == "definition" && !relation_name.startsWith("Termes ayant")){
        new_card_body_text_list_data.innerHTML = entites
        new_card_body_text_raw.innerHTML = entites
        return
    }

    // Entités en colonne
    let pagination_container = $('#list_'+relation_code);
    let data_container = $('#list_data_'+relation_code);

    pagination_container.pagination({
      dataSource: entites_split,
      callback: function(data, pagination) {
        let html = '<ul>';
        $.each(data, function(index, item){
            html += '<li>'+ item +'</li>';
        });
        html += '</ul>';
        data_container.html(html);
        }
    })


    // entites en ligne
    if (entites_split.length <= 10){
        new_card_body_text_raw.innerHTML = entites
    } else {
        let html_less = ""
        let html_more = ""
        for (let i=0; i<10; i++){
            html_less += entites_split[i] + ", "
        }
        new_card_body_text_raw_less.innerHTML = html_less
        new_card_body_text_raw_dots.innerHTML += "..."

        for (let j=10; j<entites_split.length-1; j++){
            html_more += entites_split[j] + ", "
        }
        new_card_body_text_raw_more.innerHTML = html_more + entites_split[entites_split.length-1]
    }
}

// Historique
function getHistorique() {
    let url = SERVER_URL+URL_HISTORIQUE
    
    let request = new XMLHttpRequest();
    request.open('GET', url, true)
    
    request.onload = function() {
        historique = addLinks(request.responseText.split('\n'))
        createCard("historique", "historique", "Historique", historique, "La liste des derniers mots recherchés par les utilisateurs")
    }
    request.send(null)
}

// Relations
function getRelation() {
    console.log( "getRelation()" );
    let url = SERVER_URL+URL_RELATION
    
    let request = new XMLHttpRequest();
    request.open('GET', url, true)
    
    request.onload = function() {
        relations = request.responseText.split('\n')
        createRelationsMenu()
    }
    request.send(null)
}

function createRelationsMenu(){
    relations.forEach(relation => {
        let splited = relation.split(';');
        if(document.getElementById(splited[0]+"_menu_deroulant") == undefined){
            let div = document.createElement('option')
            div.innerText = splited[1]
            div.setAttribute("id", splited[0]+"_menu_deroulant")
            div.setAttribute("value", splited[0]);
            document.getElementById("champ-recherche-relation").appendChild(div);
        }
    })
}

// Autocomplétion
function getAutocompletion(mot, response) {
    let url = SERVER_URL+URL_AUTOCOMPLETE.replace('%s', mot)
    autocomplete_request = new XMLHttpRequest();
    autocomplete_request.open('GET', url, true)
    
    autocomplete_request.onload = function() {
        mots_autocomplete = []
        autocomplete_request.responseText.replace(/\"/g,'')
        .replace(/\[/g,'')
        .replace(/\]/g,'')
        .split(',')
        .forEach(mot => {
            if (mot != ""){
                mots_autocomplete.push(mot)
            }
        })
        response(mots_autocomplete)
    }
    autocomplete_request.send(null)
}

function isBlank(mot){
    let blank = "^\\s+$";
    let e = mot.toString().replace(/\s/g,"")
    return (e == "\n" || e == "\n\n" || e == undefined || e == "" || e == " " || e.startsWith(blank) || e == "\t")
}

function changeVisuel(){
    console.log("changeVisuel")
    // Visuel = 0 pour raw ou 1 pour list
    let visuel = document.querySelector('input[name="affichage"]:checked').value

    if (visuel == "raw"){
        show("raw")
        hide("list")
    }else{
        show("list")
        hide("raw")
    }
}

function show(section){
    let obj = document.getElementsByName(section);
    obj.forEach(e => {
        if(e.style.display == "none"){
            e.style.display = "block"
        }
    })
}

function hide(section){
    let obj = document.getElementsByName(section);
    obj.forEach(e => {
        if(e.style.display == "block"){
            e.style.display = "none"
        }
    })
}

function readMore(code){
    var dots = document.getElementById("dots_"+code);
    var moreText = document.getElementById("more_"+code);
    var btnText = document.getElementById("button_"+code);
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Voir plus";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Voir moins";
      moreText.style.display = "inline";
    }
}
